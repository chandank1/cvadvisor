/** @type {import('next').NextConfig} */
const nextConfig = {
  swcMinify: true,
  basePath: "/advisor",
  assetPrefix: "/advisor",
  trailingSlash: true,
  images: {
    domains: [
      "collegevidyanew.s3.amazonaws.com",
      "d1aeya7jd2fyco.cloudfront.net",
    ],
  },
};

module.exports = nextConfig;

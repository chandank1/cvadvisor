import axios from "axios";

export default function handler(req, res) {
  axios
    .post(
      "https://api.razorpay.com/v1/orders",
      {
        amount: req.body.amount,
        currency: req.body.currency,
        receipt: req.body.receipt,
      },
      {
        headers: {
          "content-type": "application/json",
        },
        auth: {
          username: process.env.NEXT_PUBLIC_RAZORPAY_KEY_ID,
          password: process.env.NEXT_PUBLIC_RAZORPAY_KEY_SECRET,
        },
      }
    )
    .then((response) => {
      res.status(200).send(response.data);
    })
    .catch((error) => {
      res.status(500).send(error);
    });
}

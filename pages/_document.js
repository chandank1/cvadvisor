import { Html, Head, Main, NextScript } from "next/document";

export default function Document() {
  return (
    <Html
      xmlns="https://www.w3.org/1999/xhtml"
      dir="ltr"
      lang="en-US"
      translate="no"
    >
      <Head />
      <body>
        <Main />
        <NextScript />
      </body>
    </Html>
  );
}

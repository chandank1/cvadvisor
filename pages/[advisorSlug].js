import { useRouter } from "next/router";
import { Col, Container, Modal, Row, Tab, Tabs } from "react-bootstrap";
import Header from "../Components/Global/Header";
import {
  getCounsellorData,
  getCounsellorSlugs,
} from "../services/AdvisorService";
import { Button, Progress, Text } from "@nextui-org/react";
import Rating from "../Components/Global/Rating";
import { CircularProgressbar } from "react-circular-progressbar";
import UniversitySlider from "../Components/Global/UniversitySlider";
import Link from "next/link";
import LatestCouncelling from "../Components/Global/LatestCouncelling";
import CallSection from "../Components/Global/CallSection";
import ServicesOffered from "../Components/Global/ServicesOffered";
import RatingReview from "../Components/Global/RatingReview";
import { useEffect, useState } from "react";
import { youtubeParser } from "/utils/misc";
import Footer from "../Components/Global/Footer";
import Head from "next/head";
import Image from "next/image";

export async function getStaticPaths() {
  const allSlugs = await getCounsellorSlugs().then((response) => response.data);

  const paths = allSlugs.map((advisorSlug) => {
    return {
      params: {
        advisorSlug: advisorSlug.slug,
      },
    };
  });

  return { paths, fallback: true };
}

export async function getStaticProps(context) {
  const advisorSlug = context.params.advisorSlug;

  const counsellorData = await getCounsellorData(advisorSlug).then(
    (response) => response.data.university_data[0]
  );

  if (!counsellorData) {
    return {
      notFound: true,
    };
  }

  return {
    props: {
      counsellorData: counsellorData,
    },
    revalidate: 10,
  };
}

const AdvisorDetail = ({ counsellorData }) => {
  const router = useRouter();

  const percentage = 766;
  const [show, setShow] = useState(false);

  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

  useEffect(() => {
    if (router.isReady) {
      if (router.query.wr) {
        document.getElementById("write-review-button").click();
      }
    }
  }, [router]);

  if (router.isFallback) {
    return <div>Loading...</div>;
  }

  return (
    <>
      <Head>
        <title>{counsellorData.name} - Know Your Advisor.</title>
      </Head>
      <Header />
      <Container className="mb-3 mb-md-5">
        <Row>
          <Col className="col-12 col-lg-5">
            <div className="bg-white rounded shadow-1 p-4 pt-5 mb-2 position-relative">
              <div className="d-flex align-items-center row">
                <div className="position-relative col-4">
                  <Image
                    src={
                      counsellorData.profile_picture
                        ? counsellorData.profile_picture
                        : "/advisor/Images/logo.png"
                    }
                    width={140}
                    height={100}
                    alt="profile"
                    priority
                    className="rounded profile_pic"
                  />
                </div>
                <div className="col-8">
                  <Text
                    h6
                    css={{
                      textGradient: "45deg, $blue600 -20%, $pink600 50%",
                    }}
                  >
                    Welcome to College Vidya
                  </Text>
                  <h4 className="m-0 d-flex align-items-center gap-2">
                    {counsellorData.name}
                    <img
                      src={"/advisor/Images/badge.webp"}
                      width={20}
                      height={20}
                      alt="badge"
                    />
                  </h4>
                  <p className="fs-13 mb-1">({counsellorData.designation})</p>
                  <Rating
                    allReviews={counsellorData.review}
                    avgRating={counsellorData.rating}
                  />
                </div>
              </div>
              <div className="d-flex mt-3 align-items-center row">
                <div className="col-4 text-center">
                  <div className="mx-auto" style={{ width: 50, height: 50 }}>
                    <CircularProgressbar
                      value={percentage}
                      text={`${counsellorData.admission_done}`}
                    />
                  </div>
                  <p className="m-0 fs-14 mt-2">Admissions Done</p>
                  <p
                    className="m-0 fs-12 d-none d-lg-block"
                    style={{ color: "#ffa500" }}
                  >
                    (in following universities)
                  </p>
                </div>
                <div className="col-8">
                  <p className="m-0 fs-14 fw-bold mb-1">Demographics</p>
                  <p className="mb-1 fs-14 primarytext d-flex justify-content-between">
                    <span>
                      <Image
                        src={"/advisor/Images/geo-alt.svg"}
                        width={14}
                        height={14}
                        alt="location"
                      />{" "}
                      {counsellorData.address}
                    </span>{" "}
                  </p>
                  <p className="m-0 fs-14  d-flex justify-content-between">
                    <span>
                      <Image
                        src={"/advisor/Images/translate.svg"}
                        width={14}
                        height={14}
                        alt="location"
                      />{" "}
                      {counsellorData.language.split(",").join(", ")}
                    </span>{" "}
                  </p>
                  <p className="m-0 fs-14  d-flex justify-content-between">
                    <span>
                      <Image
                        src={"/advisor/Images/status.svg"}
                        width={14}
                        height={14}
                        alt="location"
                      />{" "}
                      {counsellorData.marital_status}
                    </span>{" "}
                  </p>
                </div>
              </div>
              <UniversitySlider
                counsellorUniversities={counsellorData.universities}
              />
              <p className="m-0 fs-14 fw-bold mb-1 mt-4">Experience</p>
              <div className="d-flex gap-4 align-items-center">
                <Progress
                  value={70}
                  size="sm"
                  color="primary"
                  status="secondary"
                />
                <p className="m-0" style={{ width: "100px" }}>
                  {counsellorData.experience} Years
                </p>
              </div>

              <div className="d-flex gap-3 mt-3">
                <Link
                  href={`tel:${
                    counsellorData.process === "Lingayas"
                      ? "1800-419-6222"
                      : (counsellorData.process = "CMSED"
                          ? "1800-309-3044"
                          : "1800-420-5757")
                  }`}
                  className="w-100"
                  style={{ maxWidth: "100%" }}
                >
                  <Button
                    flat
                    color="primary"
                    className="w-100"
                    style={{ minWidth: "auto" }}
                  >
                    <Image
                      src={"/advisor/Images/telephone.svg"}
                      width={16}
                      height={16}
                      alt="video"
                      style={{ marginTop: "-3px" }}
                    />{" "}
                    <span className="ms-1">Audio Call</span>
                  </Button>
                </Link>
                <Button
                  onClick={handleShow}
                  flat
                  color="success"
                  className="w-100"
                  style={{ minWidth: "auto" }}
                >
                  <Image
                    src={"/advisor/Images/video.svg"}
                    width={16}
                    height={16}
                    alt="video"
                    style={{ marginTop: "-3px" }}
                  />{" "}
                  <span className="ms-1">Video Call</span>
                </Button>
              </div>

              <div
                className="position-absolute mt-3 top-0 end-0 text-white fs-12 px-2 shadow-sm"
                style={{
                  backgroundColor: "#00d8a1",
                  borderRadius: "10px 0px 0 10px",
                  lineHeight: "25px",
                }}
              >
                <img
                  src={"/advisor/Images/medal.png"}
                  width={20}
                  height={20}
                  alt="medal"
                />{" "}
                Top 1% Advisors
              </div>
            </div>

            <LatestCouncelling />
            <div className="d-none d-lg-block">
              <CallSection />
            </div>
          </Col>
          <Col className="col-lg-7">
            <div className="shadow-1 rounded p-4 mb-2 ">
              <div>
                <span
                  className="d-block"
                  id="about"
                  style={{ paddingTop: "90px", marginTop: "-90px" }}
                ></span>
                <h3>About Me</h3>

                <p>
                  Hello Learner,{" "}
                  {counsellorData.slug !== "senior-counsellor"
                    ? `my name is ${counsellorData.name}.`
                    : ""}{" "}
                  I would like to tell you that I completed my{" "}
                  {counsellorData.qualifications.split(",")[0]} in 2016 and also
                  did Postgraduate Diploma in Career Counseling. I have
                  dedicated {counsellorData.experience} years of my life to
                  counseling students. I really love this as it makes an impact
                  in one&apos;s life and bring happiness to the face 🙂. Apart
                  from this, I also love{" "}
                  {counsellorData.hobbies.split(",").join(" and ")}.
                </p>
              </div>
              <span
                className="d-block"
                id="qualification"
                style={{ paddingTop: "90px", marginTop: "-90px" }}
              ></span>
              <h3 className="mb-3">My Qualifications</h3>
              <div className="d-flex gap-2 mb-2">
                {counsellorData.qualifications.split(",").map((qual, index) => (
                  <p
                    key={index}
                    className="px-3 py-2 rounded"
                    style={{ backgroundColor: "aliceblue" }}
                  >
                    {qual}
                  </p>
                ))}
              </div>
              <span
                className="d-block"
                id="services-offered"
                style={{ paddingTop: "90px", marginTop: "-90px" }}
              ></span>
              <h3 className="mb-3">Services Offered By Me</h3>
              <ServicesOffered />
              <span
                className="d-block"
                id="student-say"
                style={{ paddingTop: "90px", marginTop: "-90px" }}
              ></span>
              <h3 className="mb-3">What My Students Say</h3>
              <Tabs defaultActiveKey="first">
                <Tab eventKey="first" title="Testimonials">
                  {counsellorData.textual_testimonials.map((tt) => (
                    <div className="px-3 mt-4" key={tt.id}>
                      <p className="mb-1 text-dark fw-bold">{tt.witness}</p>
                      <p className="text-secondary fs-15">{tt.testimonial}</p>
                    </div>
                  ))}
                </Tab>
                <Tab eventKey="second" title="Video Testimonials">
                  <div className="px-3 mt-4 d-flex flex-wrap">
                    <div className="position-relative p-4 rounded mb-3">
                      <video
                        width={250}
                        height={200}
                        src={`https://collegevidya.com/images/feedbacks/review2.mp4`}
                        controls
                      />
                      <div className="px-3 pb-3">
                        <p className="fs-12 mt-3 mb-1 text-uppercase text-primary">
                          - RAMAN GUPTA
                        </p>
                      </div>
                    </div>
                    <div className="position-relative p-4 rounded mb-3">
                      <video
                        width={250}
                        height={200}
                        src={`https://collegevidya.com/images/feedbacks/review3.mp4`}
                        controls
                      />
                      <div className="px-3 pb-3">
                        <p className="fs-12 mt-3 mb-1 text-uppercase text-primary">
                          - RUCHIKA YADAV
                        </p>
                      </div>
                    </div>
                    {/* {counsellorData.visual_testimonials.map((vt) => (
                      <div
                        className="position-relative p-4 rounded mb-3"
                        key={vt.id}>
                        <iframe
                          width={250}
                          height={200}
                          src={`https://www.youtube.com/embed/${youtubeParser(
                            vt.video_link
                          )}`}
                          title="YouTube video player"
                          frameBorder="0"
                          allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                          allowFullScreen></iframe>
                        <div className="px-3 pb-3">
                          <p className="fs-12 mt-3 mb-1 text-uppercase text-primary">
                            - {vt.witness}
                          </p>
                        </div>
                      </div>
                    ))} */}
                  </div>
                </Tab>
              </Tabs>
              <span
                className="d-block"
                id="reviews"
                style={{ paddingTop: "90px", marginTop: "-90px" }}
              ></span>
              <h3 className="mb-3 mt-3">Rating & Reviews</h3>
              <Rating
                allReviews={counsellorData.review}
                avgRating={counsellorData.rating}
              />
              <RatingReview
                counsellorID={counsellorData.id}
                counsellorData={counsellorData}
                allReviews={[
                  {
                    id: 1,
                    name: "Prakash Jha",
                    email: "chandansingh2599@gmail.com",
                    thoughts:
                      "I'm glad that CV counselor has helped me in getting the right online university. Also, the best part is that they will help me throughout the course.",
                    rating: 5.0,
                    payment_id: "pay_L25ncnHza7cWHX",
                    created_at: "2023-01-09T13:18:49.082080Z",
                    status: 1,
                    counselor: 2,
                  },
                  {
                    id: 2,
                    name: "Sonia Kapoor",
                    email: "chandansingh2599@gmail.com",
                    thoughts:
                      "With the help and guidance of College Vidya Counselor I finally selected a university for myself. There guidance really works and I recommend other too.",
                    rating: 5.0,
                    payment_id: "pay_L25ncnHza7cWHX",
                    created_at: "2023-01-09T13:18:49.082080Z",
                    status: 1,
                    counselor: 2,
                  },
                ]}
              />
            </div>
          </Col>
          <div className="d-block d-lg-none">
            <CallSection />
          </div>
        </Row>
      </Container>
      <Footer />
      <Modal show={show} onHide={handleClose} centered>
        <Modal.Header closeButton className="border-0"></Modal.Header>
        <Modal.Body className="text-center">
          <Image
            src={"/advisor/Images/Video call-pana.png"}
            width={300}
            height={280}
            alt="video chat"
          />
          <h3>
            Get <span className="text-primary">Real Experts</span> on your side
          </h3>
          <p>
            Before venturing to seek career counseling, answer a few basic
            questions so we can connect you with our best expert counselor for
            personalised guidance and mentorship.
          </p>
          <div className="d-flex justify-content-center mb-3">
            <Link href="https://collegevidya.com/suggest-me-an-university/"><Button flat color="primary" auto>
              Let&apos;s Start
            </Button></Link>
          </div>
        </Modal.Body>
      </Modal>
    </>
  );
};

export default AdvisorDetail;

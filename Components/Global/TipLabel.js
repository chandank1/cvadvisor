import Image from "next/image";
import React from "react";

const TipLabel = (props) => {
  return (
    <>
      <div className="d-flex align-items-center">
        {props.smiley}{" "}
        {props.image ? (
          <Image
            src={"/advisor/Images/rupee.svg"}
            width={16}
            height={16}
            alt="rupee"
          />
        ) : null}{" "}
        {props.amount}
      </div>
    </>
  );
};

export default TipLabel;

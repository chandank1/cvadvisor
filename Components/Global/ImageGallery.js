import "primeicons/primeicons.css";
import "primereact/resources/themes/lara-light-indigo/theme.css";
import "primereact/resources/primereact.css";

import React, { useState, useEffect, useRef } from "react";
import { Galleria } from "primereact/galleria";

const ImageGallery = () => {
  const [images, setImages] = useState([
    {
      itemImageSrc: "/advisor/Gallery/pic1.jpg",
      thumbnailImageSrc: "/advisor/Gallery/pic1.jpg",
      alt: "College vidya",
      title: "Title 1",
    },
    {
      itemImageSrc: "/advisor/Gallery/pic2.jpg",
      thumbnailImageSrc: "/advisor/Gallery/pic2.jpg",
      alt: "College vidya",
      title: "Title 2",
    },
    {
      itemImageSrc: "/advisor/Gallery/pic3.jpg",
      thumbnailImageSrc: "/advisor/Gallery/pic3.jpg",
      alt: "College vidya",
      title: "Title 3",
    },
    {
      itemImageSrc: "/advisor/Gallery/pic4.jpg",
      thumbnailImageSrc: "/advisor/Gallery/pic4.jpg",
      alt: "College vidya",
      title: "Title 4",
    },
    {
      itemImageSrc: "/advisor/Gallery/pic5.jpg",
      thumbnailImageSrc: "/advisor/Gallery/pic5.jpg",
      alt: "College vidya",
      title: "Title 7",
    },
    {
      itemImageSrc: "/advisor/Gallery/pic6.jpg",
      thumbnailImageSrc: "/advisor/Gallery/pic6.jpg",
      alt: "College vidya",
      title: "Title 8",
    },
    {
      itemImageSrc: "/advisor/Gallery/pic7.jpg",
      thumbnailImageSrc: "/advisor/Gallery/pic7.jpg",
      alt: "College vidya",
      title: "Title 8",
    },
    {
      itemImageSrc: "/advisor/Gallery/pic8.jpg",
      thumbnailImageSrc: "/advisor/Gallery/pic8.jpg",
      alt: "College vidya",
      title: "Title 8",
    },
    {
      itemImageSrc: "/advisor/Gallery/pic9.jpg",
      thumbnailImageSrc: "/advisor/Gallery/pic9.jpg",
      alt: "College vidya",
      title: "Title 8",
    },
  ]);
  const [activeIndex, setActiveIndex] = useState(0);

  const galleria2 = useRef(null);
  const galleria3 = useRef(null);

  const responsiveOptions = [
    {
      breakpoint: "1024px",
      numVisible: 5,
    },
    {
      breakpoint: "768px",
      numVisible: 3,
    },
    {
      breakpoint: "560px",
      numVisible: 1,
    },
  ];
  const responsiveOptions2 = [
    {
      breakpoint: "1500px",
      numVisible: 5,
    },
    {
      breakpoint: "1024px",
      numVisible: 3,
    },
    {
      breakpoint: "768px",
      numVisible: 2,
    },
    {
      breakpoint: "560px",
      numVisible: 1,
    },
  ];

  const itemTemplate = (item) => {
    return (
      <img
        src={item.itemImageSrc}
        onError={(e) =>
          (e.target.src =
            "https://www.primefaces.org/wp-content/uploads/2020/05/placeholder.png")
        }
        alt={item.alt}
        style={{ width: "100%", display: "block" }}
      />
    );
  };

  const thumbnailTemplate = (item) => {
    return (
      <img
        src={item.thumbnailImageSrc}
        onError={(e) =>
          (e.target.src =
            "https://www.primefaces.org/wp-content/uploads/2020/05/placeholder.png")
        }
        alt={item.alt}
        style={{ display: "block" }}
      />
    );
  };

  return (
    <>
      <div>
        <Galleria
          ref={galleria2}
          value={images}
          responsiveOptions={responsiveOptions}
          numVisible={7}
          style={{ maxWidth: "850px" }}
          circular
          fullScreen
          showItemNavigators
          showThumbnails={false}
          item={itemTemplate}
          thumbnail={thumbnailTemplate}
        />

        <div className="card p-3">
          <Galleria
            ref={galleria3}
            value={images}
            responsiveOptions={responsiveOptions}
            numVisible={7}
            style={{ maxWidth: "850px" }}
            activeIndex={activeIndex}
            onItemChange={(e) => setActiveIndex(e.index)}
            circular
            fullScreen
            showItemNavigators
            showThumbnails={false}
            item={itemTemplate}
            thumbnail={thumbnailTemplate}
          />

          <div className="row row-cols-3">
            {images &&
              images.map((image, index) => {
                let imgEl = (
                  <img
                    src={image.thumbnailImageSrc}
                    alt={image.alt}
                    className="w-100 rounded"
                    style={{ cursor: "pointer" }}
                    onClick={() => {
                      setActiveIndex(index);
                      galleria3.current.show();
                    }}
                  />
                );

                return (
                  <div className="col mb-3" key={index}>
                    {imgEl}
                  </div>
                );
              })}
          </div>
          <p
            className="m-0 fs-12 text-primary cursor-pointer"
            onClick={() => galleria2.current.show()}
          >
            View All Photos
          </p>
        </div>
      </div>
    </>
  );
};

export default ImageGallery;

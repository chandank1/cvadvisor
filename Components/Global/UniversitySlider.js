import React, { useEffect, useState } from "react";
import { Swiper, SwiperSlide } from "swiper/react";
import "swiper/css";
import { Navigation, Autoplay } from "swiper";
import "swiper/css/navigation";
import "swiper/css/autoplay";
import Image from "next/image";
import universityimg from "../../public/Images/NMIMS.jpeg";
import { getUniversities } from "../../services/AdvisorService";

const UniversitySlider = ({ counsellorUniversities }) => {
  const [universityList, setUniversityList] = useState([]);

  useEffect(() => {
    getUniversities().then((response) => {
      setUniversityList(response.data);
    });
  }, []);

  return (
    <div className="position-relative">
      <img
        src={"/advisor/Images/curved-arrow.png"}
        width={35}
        height={35}
        alt="curved"
        className="position-absolute"
        style={{ bottom: "20px", left: "12px" }}
      />
      <Swiper
        spaceBetween={20}
        slidesPerView="auto"
        loop={false}
        navigation={false}
        autoplay={{
          delay: "0s",
          disableOnInteraction: false,
        }}
        interval={1000}
        speed={5000}
        className="mt-4 ms-5"
        style={{ userSelect: "none" }}
        breakpoints={{
          "@0.00": {
            slidesPerView: 3,
            spaceBetween: 20,
          },
          "@0.75": {
            slidesPerView: 3,
            spaceBetween: 20,
          },
          "@1.00": {
            slidesPerView: 3,
            spaceBetween: 20,
          },
          "@1.75": {
            slidesPerView: 4,
            spaceBetween: 20,
          },
        }}
        modules={[Navigation, Autoplay]}>
        {universityList.map((list, index) => {
          if (
            counsellorUniversities &&
            JSON.parse(counsellorUniversities).includes(list.id)
          ) {
            return (
              <SwiperSlide key={index}>
                <div className="border rounded p-1">
                  <Image
                    src={list.logo}
                    width={120}
                    height={40}
                    layout="responsive"
                    alt="university logo"
                  />
                </div>
              </SwiperSlide>
            );
          }
        })}
      </Swiper>
    </div>
  );
};

export default UniversitySlider;

import Logo from "./Logo";
import WhyCV from "./WhyCV";
import Container from "react-bootstrap/Container";
import Nav from "react-bootstrap/Nav";
import Navbar from "react-bootstrap/Navbar";
import Offcanvas from "react-bootstrap/Offcanvas";

const Header = () => {
  return (
    <>
   <Container>
   <div className="d-flex justify-content-between align-items-center py-3">
    <Logo />
    <WhyCV />
    </div>
   </Container>
      {/* <Navbar bg="white" expand="lg" className="mb-3 sticky-top shadow-sm">
        <Container>
          <Navbar.Brand>
            <Logo />
          </Navbar.Brand>
          <Navbar.Toggle aria-controls={`offcanvasNavbar-expand-lg`} />
          <Navbar.Offcanvas
            id={`offcanvasNavbar-expand-lg`}
            aria-labelledby={`offcanvasNavbarLabel-expand-lg`}
            placement="end"
          >
            <Offcanvas.Header closeButton>
              <Offcanvas.Title id={`offcanvasNavbarLabel-expand-lg`}>
                <Logo />
              </Offcanvas.Title>
            </Offcanvas.Header>
            <Offcanvas.Body>
              <Nav className="justify-content-end flex-grow-1 pe-3">
                <Nav.Link href="#about" className="text-dark">
                  About
                </Nav.Link>
                <Nav.Link href="#qualification" className="text-dark">
                  Qualification
                </Nav.Link>
                <Nav.Link href="#services-offered" className="text-dark">
                  Services Offered
                </Nav.Link>
                <Nav.Link href="#student-say" className="text-dark">
                  Student Say
                </Nav.Link>
                <Nav.Link href="#reviews" className="text-dark">
                  Rating & Reviews
                </Nav.Link>
              </Nav>
              <Nav>
                <WhyCV />
              </Nav>
            </Offcanvas.Body>
          </Navbar.Offcanvas>
        </Container>
      </Navbar> */}
    </>
  );
};

export default Header;

import React from "react";
import ReactStars from "react-stars";

const Rating = ({ allReviews, avgRating }) => {
  return (
    <div className="d-flex align-items-center gap-1">
      <ReactStars
        count={5}
        size={15}
        value={4}
        edit={false}
        color1={"#ddd"}
        color2={"#ffd700"}
      />
      <span className="fs-12 ms-2">(345)</span>
    </div>
  );
};

export default Rating;

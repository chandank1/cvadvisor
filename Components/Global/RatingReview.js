import React, { useState } from "react";
import Modal from "react-bootstrap/Modal";
import ReviewCard from "./ReviewCard";
import Image from "next/image";
import { Loading, Textarea } from "@nextui-org/react";
import { Input, Spacer } from "@nextui-org/react";
import { Button } from "@nextui-org/react";
import StarRatings from "react-star-ratings";
import Form from "react-bootstrap/Form";
import TipLabel from "./TipLabel";
import * as Yup from "yup";
import { yupResolver } from "@hookform/resolvers/yup";
import { useForm } from "react-hook-form";
import { addReview } from "../../services/AdvisorService";
import { useRouter } from "next/router";
import useRazorpay from "react-razorpay";
import axios from "axios";

const RatingReview = ({ counsellorID, counsellorData, allReviews }) => {
  const router = useRouter();
  const Razorpay = useRazorpay();

  const [RateMsg, setRateMsg] = useState(0);
  const [RateColor, setRateColor] = useState(null);
  const [HoverRating, setHoverRating] = useState(null);
  const [HoverColor, setHoverColor] = useState(null);
  const [postingReview, setPostingReview] = useState(false);

  const ratingChanged = (newRating) => {
    setRateColor(
      newRating === 1
        ? "red"
        : newRating === 2
        ? "red"
        : newRating === 3
        ? "orange"
        : newRating === 4
        ? "green"
        : newRating === 5
        ? "green"
        : null
    );
    setRateMsg(newRating);
    setValue("rating", newRating, { shouldValidate: true });
  };

  const onHoverColor = (newRating) => {
    setHoverRating(newRating);
    setHoverColor(
      newRating === 1
        ? "red"
        : newRating === 2
        ? "red"
        : newRating === 3
        ? "orange"
        : newRating === 4
        ? "green"
        : newRating === 5
        ? "green"
        : null
    );
  };

  const [show, setShow] = useState(false);

  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

  const [WriteReview, setWriteReview] = useState(false);

  const validationSchema = Yup.object().shape({
    name: Yup.string().required("Name is required"),
    email: Yup.string().email("Invalid email").required("Email is required"),
    rating: Yup.number()
      .typeError("Invalid rating")
      .required("Rating is required"),
    thoughts: Yup.string().required("Review is required"),
    counsellorTip: Yup.number().typeError("Invalid amount").nullable(),
    customTip: Yup.number()
      .min("1", "Tip amount can't be lower than ₹1")
      .max("1999", "Tip amount can't be more than ₹1999")
      .typeError("Invalid amount")
      .nullable(),
  });

  const {
    register,
    setValue,
    getValues,
    setError,
    formState: { errors },
    handleSubmit,
  } = useForm({
    defaultValues: {
      counsellorTip: null,
      customTip: 1,
    },
    mode: "onChange",
    resolver: yupResolver(validationSchema),
  });

  async function createOrder(params) {
    const order = await axios
      .post("/advisor/api/razorpay", {
        amount: params.amount,
        currency: params.currency,
        receipt: params.receipt,
      })
      .then((response) => response.data.id);

    return order;
  }

  const onSubmit = async (data) => {
    setPostingReview(true);
    let finalTip = null;
    if (data.counsellorTip === -1) {
      finalTip = data.customTip;
    } else {
      finalTip = data.counsellorTip;
    }

    let formData = new FormData();
    formData.append("uid", counsellorID);
    formData.append("name", data.name);
    formData.append("email", data.email);
    formData.append("rating", data.rating);
    formData.append("thoughts", data.thoughts);

    if (finalTip) {
      const order = await createOrder({
        amount: finalTip * 100,
        currency: "INR",
        receipt: "receipt_" + new Date().getTime(),
      });

      const options = {
        key: process.env.NEXT_PUBLIC_RAZORPAY_KEY_ID,
        amount: finalTip * 100,
        currency: "INR",
        name: "College Vidya - Know Your Advisor",
        description: `Tip for ${counsellorData.name} (${counsellorData.emp_id})`,
        order_id: order.id,
        handler: function (response) {
          formData.append("payment_id", response.razorpay_payment_id);
          addReview(formData)
            .then(() => {
              router.reload();
            })
            .catch(() => {
              setPostingReview(false);
            });
        },
        prefill: {
          name: data.name,
          email: data.email,
        },
        theme: {
          color: "#0074d7",
        },
      };

      const rzp1 = new Razorpay(options);

      rzp1.on("payment.failed", function (response) {
        iziToast.error({
          title: "Payment failed",
          message: response,
          timeout: 2000,
          position: "topRight",
        });
      });

      rzp1.open();
    } else {
      addReview(formData)
        .then(() => {
          router.reload();
        })
        .catch(() => {
          setPostingReview(false);
        });
    }
  };

  return (
    <>
      {allReviews.slice(0, 2).map((list, index) => (
        <ReviewCard data={list} index={index} key={index} />
      ))}

      <div className="d-flex justify-content-between">
        <Button
          auto
          color="warning"
          rounded
          flat
          onClick={() => {
            handleShow();
            setWriteReview(true);
          }}>
          <span id="write-review-button" className="me-1">
            Write Review
          </span>{" "}
          <Image
            src={"/advisor/Images/pencil.svg"}
            width={10}
            height={10}
            alt="write review"
          />
          {""}
        </Button>
        <p
          className="fs-14 text-end text-primary cursor-pointer m-0 d-flex align-items-center"
          onClick={() => {
            handleShow();
            setWriteReview(false);
          }}>
          View All Reviews
        </p>
      </div>
      <Modal
        show={show}
        onHide={handleClose}
        backdrop="static"
        centered
        scrollable>
        <Modal.Header closeButton className="border-0">
          <Modal.Title>
            {WriteReview ? (
              <span>Rate Your Counsellor</span>
            ) : (
              <span>All Reviews</span>
            )}
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          {WriteReview ? (
            <>
              <form onSubmit={handleSubmit(onSubmit)}>
                <div className="d-flex align-items-center mt-1 justify-content-between">
                  <span
                    onMouseLeave={() => {
                      setHoverColor(null);
                      setHoverRating(null);
                    }}>
                    <StarRatings
                      rating={RateMsg}
                      starRatedColor={RateColor}
                      starHoverColor={HoverColor}
                      starDimension="25px"
                      starSpacing="3px"
                      changeRating={ratingChanged}
                      numberOfStars={5}
                      name="rating"
                      onHover={(sr) => onHoverColor(sr)}
                    />
                    <span className="p-error fs-12 d-block">
                      {errors["rating"]?.message}
                    </span>
                  </span>

                  <span
                    className="fs-12 ms-2"
                    style={{ color: HoverColor ? HoverColor : RateColor }}>
                    {HoverColor
                      ? HoverColor === "red"
                        ? "Poor"
                        : HoverColor === "orange"
                        ? "Average"
                        : HoverColor === "green"
                        ? "Excellent"
                        : null
                      : RateMsg === 1
                      ? "Poor"
                      : RateMsg === 2
                      ? "Poor"
                      : RateMsg === 3
                      ? "Average"
                      : RateMsg === 4
                      ? "Excellent"
                      : RateMsg === 5
                      ? "Excellent"
                      : null}
                  </span>
                </div>
                <p>
                  {" "}
                  <span className="fs-12 ps-1">
                    {HoverRating
                      ? HoverRating === 1
                        ? "What did you hate? 😞"
                        : HoverRating === 2
                        ? "What was not upto the mark? 😐"
                        : HoverRating === 3
                        ? "What was not upto the mark? 😐"
                        : HoverRating === 4
                        ? "What did you like? 😄"
                        : HoverRating === 5
                        ? "What did you love? 😍"
                        : null
                      : RateMsg === 1
                      ? "What did you hate? 😞"
                      : RateMsg === 2
                      ? "What was not upto the mark? 😐"
                      : RateMsg === 3
                      ? "What was not upto the mark? 😐"
                      : RateMsg === 4
                      ? "What did you like? 😄"
                      : RateMsg === 5
                      ? "What did you love? 😍"
                      : null}
                    {}
                  </span>
                </p>
                <div className="d-flex gap-3">
                  <div>
                    <Input
                      {...register("name")}
                      type="text"
                      placeholder="Name"
                      shadow={false}
                      fullWidth
                    />
                    <span className="p-error fs-12 d-block">
                      {errors["name"]?.message}
                    </span>
                  </div>

                  <div>
                    <Input
                      {...register("email")}
                      type="email"
                      placeholder="Email"
                      shadow={false}
                      fullWidth
                    />
                    <span className="p-error fs-12 d-block">
                      {errors["email"]?.message}
                    </span>
                  </div>
                </div>

                <Spacer y={0.8} />
                <Textarea
                  {...register("thoughts")}
                  shadow={false}
                  fullWidth
                  placeholder="Write your thoughts"
                />
                <span className="p-error fs-12 d-block">
                  {errors["thoughts"]?.message}
                </span>
                <div className="d-flex align-items-center justify-content-between">
                  <p className="m-0 fs-14 fw-bold mb-3 mt-4">
                    Reward your Counsellor
                  </p>
                  {getValues("counsellorTip") ? (
                    <p
                      onClick={() =>
                        setValue("counsellorTip", null, {
                          shouldValidate: true,
                        })
                      }
                      className="m-0 fs-14 font-bold mb-3 mt-4 cursor-pointer"
                      style={{ color: "#f75d34", textDecoration: "underline" }}>
                      CLEAR
                    </p>
                  ) : null}
                </div>
                <form className="tipForm">
                  <div className="d-flex gap-2 flex-wrap">
                    <Form.Check
                      className="p-0"
                      name="tip"
                      type="radio"
                      onClick={() =>
                        setValue("counsellorTip", 99, {
                          shouldValidate: true,
                        })
                      }
                      checked={getValues("counsellorTip") === 99}
                      label={
                        <span>
                          <TipLabel
                            image={true}
                            amount="99"
                            smiley="&#128515;"
                          />
                        </span>
                      }
                      id={`radio1`}
                    />
                    <div className="position-relative">
                      <Form.Check
                        className="p-0"
                        name="tip"
                        type="radio"
                        onClick={() =>
                          setValue("counsellorTip", 199, {
                            shouldValidate: true,
                          })
                        }
                        checked={getValues("counsellorTip") === 199}
                        label={
                          <span>
                            {" "}
                            <TipLabel
                              image={true}
                              amount="199"
                              smiley="&#129321;"
                            />
                          </span>
                        }
                        id={`radio2`}
                      />
                      <span
                        className="w-100 position-absolute start-50 translate-middle-x w-75 text-white text-center text-uppercase"
                        style={{
                          fontSize: "8px",
                          backgroundColor: "#f75d34",
                          bottom: "-10px",
                          borderRadius: "10px",
                        }}>
                        Most Rewarded
                      </span>
                    </div>
                    <Form.Check
                      className="p-0"
                      name="tip"
                      type="radio"
                      onClick={() =>
                        setValue("counsellorTip", 499, {
                          shouldValidate: true,
                        })
                      }
                      checked={getValues("counsellorTip") === 499}
                      label={
                        <span>
                          <TipLabel
                            image={true}
                            amount="499"
                            smiley="&#128525;"
                          />
                        </span>
                      }
                      id={`radio3`}
                    />
                    <Form.Check
                      className="p-0"
                      name="tip"
                      type="radio"
                      onClick={() =>
                        setValue("counsellorTip", -1, {
                          shouldValidate: true,
                        })
                      }
                      label={
                        <span>
                          <TipLabel
                            image={false}
                            amount="Other"
                            smiley="&#128079;"
                          />
                        </span>
                      }
                      checked={getValues("counsellorTip") === -1}
                      id={`radio4`}
                    />
                  </div>
                </form>
                {getValues("counsellorTip") === -1 ? (
                  <div className="mt-4">
                    <Input
                      {...register("customTip")}
                      shadow={false}
                      clearable
                      contentRightStyling={false}
                      placeholder="Type your amount..."
                    />
                  </div>
                ) : null}
                <span className="p-error fs-12 d-block">
                  {errors["counsellorTip"]?.message}
                </span>
                <span className="p-error fs-12 d-block">
                  {errors["customTip"]?.message}
                </span>

                <Button
                  type="submit"
                  className="mt-3 w-100 mt-4"
                  flat
                  color="primary"
                  auto>
                  {postingReview ? (
                    <Loading
                      type="points-opacity"
                      color="currentColor"
                      size="sm"
                    />
                  ) : (
                    <>Submit</>
                  )}
                </Button>
              </form>
            </>
          ) : (
            <>
              {allReviews.map((list, index) => (
                <ReviewCard data={list} index={index} key={index} />
              ))}
            </>
          )}
        </Modal.Body>
      </Modal>
    </>
  );
};

export default RatingReview;

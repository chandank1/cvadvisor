import React, { useState } from "react";
import Image from "next/image";

const ReviewCard = (props) => {
  const [status, setStatus] = useState();

  return (
    <>
      <div className="d-flex gap-3 mt-3" key={props.data.id}>
        <div className="pt-2">
          {" "}
          <div
            className="rounded d-flex align-items-center justify-content-center"
            style={{
              width: "40px",
              height: "35px",
              padding: "2px",
              backgroundColor: "aliceblue",
            }}>
            {props.data.name.slice(0, 1).toUpperCase()}
          </div>
        </div>
        <div>
          <p className="mb-1">
            {props.data.thoughts.slice(
              0,
              status ? props.data.thoughts.length : 110
            )}
            <span
              className="ms-2 fs-12 text-primary text-decoration-underline cursor-pointer"
              onClick={() => setStatus(!status)}>
              {props.data.thoughts.length > 110
                ? status
                  ? "Read Less"
                  : "Read More"
                : null}
            </span>
          </p>
          <p className="fs-13 text-secondary">- {props.data.name}</p>
        </div>
      </div>
    </>
  );
};

export default ReviewCard;

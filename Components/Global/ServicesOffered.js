import React from "react";
import { Swiper, SwiperSlide } from "swiper/react";
import "swiper/css";
import "swiper/css/pagination";
import "swiper/css/navigation";
import { Navigation, Pagination } from "swiper";
import Image from "next/image";

const ServicesOffered = () => {
  const allList = [
    {
      para: "Finding the Right University for you based on your objective",
    },
    {
      para: "Documents Eligibility Checking and Coordination with University for Submission",
    },
    {
      para: "Finding the lowest-cost EMI provider",
    },
    {
      para: "Helping in the Disbursement of Loans if any",
    },
    {
      para: "Admission Process Assistance",
    },
    {
      para: "Post Admission Services like- Timely Delivery of LMS, Assignment Submissions Assistance ",
    },
    {
      para: "Convocation/Degree Award Process",
    },
    {
      para: "Guiding you with what other courses your batchmates are doing!",
    },
    {
      para: "Life Long Friend for your Career!",
    },
  ];
  return (
    <>
      <Swiper
        slidesPerView="auto"
        spaceBetween={30}
        navigation={true}
        loop={false}
        // pagination={{
        //   clickable: true,
        // }}
        pagination={false}
        breakpoints={{
          "@0.00": {
            slidesPerView: 1,
            spaceBetween: 20,
          },
          "@0.75": {
            slidesPerView: 2,
            spaceBetween: 20,
          },
          "@1.00": {
            slidesPerView: 3,
            spaceBetween: 20,
          },
        }}
        modules={[Pagination, Navigation]}
        className="services-offered mb-4"
        style={{ padding: "0px 10px 0px 10px" }}
      >
        {allList.map((list, index) => (
          <SwiperSlide
            className="px-3 pt-5 pb-4 rounded d-flex h-auto"
            key={index}
          >
            <div className="services-offered-list h-100">
              <p
                className="m-0 position-absolute bg-white rounded-circle top-0 d-flex align-items-center justify-content-center mt-2"
                style={{
                  width: "30px",
                  height: "30px",
                  border: "1px solid #ddd",
                }}
              >
                {index + 1}
              </p>
              <p className="d-flex gap-1">
                <Image
                  src={"/advisor/Images/double-right.svg"}
                  alt="right arrow"
                  width={16}
                  height={16}
                  className="mt-1"
                />{" "}
                {list.para}
              </p>
            </div>
          </SwiperSlide>
        ))}
      </Swiper>
    </>
  );
};

export default ServicesOffered;

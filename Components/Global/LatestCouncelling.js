import Image from "next/image";
import React from "react";

const LatestCouncelling = () => {
  return (
    <>
      <div className="shadow-1 rounded p-4 mb-3">
        <p className="m-0 h5 fw-bold mb-1 mt-2 mb-3">Latest Counselling</p>
        <div style={{ backgroundColor: "aliceblue" }} className="rounded p-3">
          <div className="d-flex gap-3 align-items-center mb-3 bg-white p-2 rounded border position-relative">
            <div>
              <div
                className="rounded d-flex align-items-center justify-content-center"
                style={{
                  fontSize: "22px",
                  width: "40px",
                  height: "35px",
                  padding: "25px",
                  backgroundColor: "aliceblue",
                }}>
                S
              </div>
            </div>
            <div>
              <p className="m-0 text-dark">Shivam Tyagi</p>
              <p className="fs-12 text-secondary m-0">Pune, Maharastra</p>
            </div>

            <div
              className="ms-auto px-2 py-1 rounded d-flex align-items-center justify-content-center flex-column"
              style={{
                backgroundColor: "#DAFBE8",
                color: "#13A452",
                border: "1px solid #b5f2d0",
              }}>
              <h5 className="m-0">10</h5>
              <p className="m-0 fs-11">mins ago</p>
            </div>
          </div>
          <div className="d-flex gap-3 align-items-center mb-3 bg-white p-2 rounded border position-relative">
            <div>
              <div
                className="rounded d-flex align-items-center justify-content-center"
                style={{
                  fontSize: "22px",
                  width: "40px",
                  height: "35px",
                  padding: "25px",
                  backgroundColor: "aliceblue",
                }}>
                T
              </div>
            </div>
            <div>
              <p className="m-0 text-dark">Tanishq Mehra</p>
              <p className="fs-12 text-secondary m-0">Jaipur, Rajasthan</p>
            </div>
            <div
              className="ms-auto px-2 py-1 rounded d-flex align-items-center justify-content-center flex-column"
              style={{
                backgroundColor: "#DAFBE8",
                color: "#13A452",
                border: "1px solid #b5f2d0",
              }}>
              <h5 className="m-0">12</h5>
              <p className="m-0 fs-11">mins ago</p>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default LatestCouncelling;

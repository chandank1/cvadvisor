import React, { useState } from "react";
import Modal from "react-bootstrap/Modal";
import { Button } from "@nextui-org/react";
import Image from "next/image";
import Logo from "./Logo";

const WhyCV = () => {
  const [show, setShow] = useState(false);

  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);
  return (
    <>
      <Button onClick={handleShow} auto flat>
        Why College Vidya{" "}
        <Image
          src={"/advisor/Images/question.svg"}
          width={20}
          height={20}
          alt="question"
        />
      </Button>
      <Modal show={show} onHide={handleClose} size="lg" centered scrollable>
        <Modal.Header closeButton>
          <Modal.Title>
            <Logo />
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <h3 className="text-center">
            why choose College Vidya for making the right decision ?
          </h3>
          <p className="text-center">
            100k+ Students Trust College Vidya for Admission to Online
            Universities
          </p>
          <div className="mx-auto col-md-12">
            <div className="mb-4 p-4 rounded aliceblue">
              <h4 className="d-flex align-items-center">
                <span
                  className="d-inline-block d-flex align-items-center justify-content-center me-2 rounded-circle bg-white"
                  style={{ width: "35px", height: "35px" }}>
                  1
                </span>{" "}
                Unbiased Suggestions
              </h4>
              <p>
                As we are a information portal and not from any university we
                are not biassed to any one and work in favour of what&apos;s
                best for the students, with college Vidya you get to explore all
                your options and select the best for you according to your
                needs.
              </p>
            </div>
            <div className="mb-4 p-4 rounded aliceblue">
              <h4 className="d-flex align-items-center">
                <span
                  className="d-inline-block d-flex align-items-center justify-content-center me-2 rounded-circle bg-white"
                  style={{ width: "35px", height: "35px" }}>
                  2
                </span>{" "}
                Save you from a Scam
              </h4>
              <p>
                When you search for a university you get 100s of websites,
                making it difficult to know which one is the official site of
                the university. This is where we come to your rescue. We save
                you from getting scammed by fake websites and guide you to the
                official site.
              </p>
            </div>
            <div className="mb-4 p-4 rounded aliceblue">
              <h4 className="d-flex align-items-center">
                <span
                  className="d-inline-block d-flex align-items-center justify-content-center me-2 rounded-circle bg-white"
                  style={{ width: "35px", height: "35px" }}>
                  3
                </span>
                Direct Payments to the University
              </h4>
              <p>
                We at college Vidya do not work as middle men which means that
                you pay the fees and any other payment directly to the
                university. We request students to pay directly to the
                university through their official website.
              </p>
            </div>
            <div className="mb-4 p-4 rounded aliceblue">
              <h4 className="d-flex align-items-center">
                <span
                  className="d-inline-block d-flex align-items-center justify-content-center me-2 rounded-circle bg-white"
                  style={{ width: "35px", height: "35px" }}>
                  4
                </span>
                Full Support
              </h4>
              <p>
                We are there for you from the start. From the day you decide to
                start your higher education via online mode till you complete
                your journey, you will find us right beside you.
              </p>
            </div>
            <div className="mb-4 p-4 rounded aliceblue">
              <h4 className="d-flex align-items-center">
                <span
                  className="d-inline-block d-flex align-items-center justify-content-center me-2 rounded-circle bg-white"
                  style={{ width: "35px", height: "35px" }}>
                  5
                </span>
                Personal Counsellors
              </h4>
              <p>
                We assign 2 personal counsellors to each student, one from the
                university and the other from college Vidya itself. Counsellors
                assigned to the student are always available to resolve any
                query at <a href="tel:18004205757"> 18004205757</a>
              </p>
            </div>
            <div className="mb-4 p-4 rounded aliceblue">
              <h4 className="d-flex align-items-center">
                <span
                  className="d-inline-block d-flex align-items-center justify-content-center me-2 rounded-circle bg-white"
                  style={{ width: "35px", height: "35px" }}>
                  6
                </span>
                Our Services
              </h4>
              <p>
                We offer our services for free so that everyone gets the right
                guidance. Our experts are available round the clock to assist
                you in your journey.
              </p>
            </div>
          </div>
        </Modal.Body>
      </Modal>
    </>
  );
};

export default WhyCV;

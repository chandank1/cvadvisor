import { Image } from "@nextui-org/react";
import Link from "next/link";
import React from "react";

const Logo = () => {
  return (
    <>
      <Link href="https://collegevidya.com">
        <Image
          src={"/advisor/Images/logo.png"}
          width={80}
          height={70}
          alt="logo"
        />
      </Link>
    </>
  );
};

export default Logo;

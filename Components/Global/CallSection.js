import Link from "next/link";
import React from "react";
import ImageGallery from "./ImageGallery";
import Image from "next/image";

const CallSection = () => {
  return (
    <>
      <div className="shadow-1 rounded p-4 mb-3">
        <div
          style={{ backgroundColor: "aliceblue" }}
          className="px-3 py-2 rounded">
          <p className="m-0 h5 fw-bold mb-1 mt-4">
            Call doesn&apos;t feel enough
          </p>
          <p className="fs-14">
            Let&apos;s chat face-to-face! Drop by our office for expert
            consultation.
          </p>
          <div className="d-flex gap-2 mb-3">
            <ImageGallery />
          </div>
        </div>
        <div>
          <p className="d-flex gap-2 mt-3">
            <Image
              className="mt-1"
              src={"/advisor/Images/geo.svg"}
              width={16}
              height={16}
              alt="location"
            />{" "}
            <Link
              className="text-dark"
              href="https://goo.gl/maps/B6dQ3pzeeztPoxzR7"
              target="_blank">
              B-136-137, Captain Vijyant Thapar Marg, B Block, Sector 2, Noida,
              Uttar Pradesh 201301
            </Link>
          </p>
          <p className="d-flex gap-2 mt-3">
            <Image
              className="mt-1"
              src={"/advisor/Images/arrow-90.svg"}
              width={16}
              height={16}
              alt="location"
            />{" "}
            <Link
              className="text-dark"
              href="https://goo.gl/maps/B6dQ3pzeeztPoxzR7"
              target="_blank">
              Get Direction
            </Link>
          </p>

          <p className="d-flex align-items-center gap-2">
            <Image
              src={"/advisor/Images/world.svg"}
              width={16}
              height={16}
              alt="location"
            />{" "}
            <span>
              <Link className="text-dark" href="https://collegevidya.com/">
                collegevidya.com
              </Link>
            </span>
          </p>
          <p className="d-flex align-items-center gap-2">
            <Image
              src={"/advisor/Images/telephone.svg"}
              width={16}
              height={16}
              alt="location"
            />{" "}
            <span>
              <Link className="text-dark" href="tel:1800-420-5757">
                1800-420-5757{" "}
                <span className="fs-12 ms-2 bg-primary rounded text-white px-1">
                  Toll Free
                </span>
              </Link>
            </span>
          </p>
          <p className="d-flex align-items-center gap-2">
            <Image
              src={"/advisor/Images/mail.svg"}
              width={16}
              height={16}
              alt="location"
            />{" "}
            <span>
              <Link className="text-dark" href="mailTo:info@collegevidya.com">
                info@collegevidya.com{" "}
              </Link>
            </span>
          </p>
        </div>
      </div>
    </>
  );
};

export default CallSection;

import React from 'react'
import Container from 'react-bootstrap/Container'

const Footer = () => {
  return (
    <>
    <div style={{backgroundColor:"#f2f2f2"}} className="py-3">
       <Container>
        <p className='m-0 text-center'>© 2023 College Vidya, Inc. All Rights Reserved.</p>
       </Container>
    </div>
    </>
  )
}

export default Footer
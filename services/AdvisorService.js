import axios from "axios";

export const getCounsellorSlugs = () => {
  return axios.get(process.env.NEXT_PUBLIC_BACKEND_URL + `counselor_slugs/`);
};

export const getCounsellorData = (counsellorID) => {
  return axios.get(
    process.env.NEXT_PUBLIC_BACKEND_URL +
      `counselor_details_slug/${counsellorID}/`
  );
};

export const getUniversities = () => {
  return axios.get(
    process.env.NEXT_PUBLIC_BACKEND_URL + "counselor_universities/"
  );
};

export const addReview = (formData) => {
  return axios.post(
    process.env.NEXT_PUBLIC_BACKEND_URL + "counselor_review/",
    formData
  );
};
